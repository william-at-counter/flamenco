// Package api provides primitives to interact with the openapi HTTP API.
//
// Code generated by github.com/deepmap/oapi-codegen version v1.9.0 DO NOT EDIT.
package api

import (
	"bytes"
	"compress/gzip"
	"encoding/base64"
	"fmt"
	"net/url"
	"path"
	"strings"

	"github.com/getkin/kin-openapi/openapi3"
)

// Base64 encoded, gzipped, json marshaled Swagger object
var swaggerSpec = []string{

	"H4sIAAAAAAAC/+y923LcOJYo+iuInBPhqpjMlCz5Ula/HLcvVaq2yxpL7jonWhVKJInMhEUCbAJUOtvh",
	"iPmI8ydnT8R+2PO0f6Dmj3ZgLQAESTAvsiWr3NMP1VaSxGVhYd0vHweJzAspmNBqcPRxoJIFyyn886lS",
	"fC5YekbVpfk7ZSopeaG5FIOjxlPCFaFEm39RRbg2f5csYfyKpWS6InrByK+yvGTleDAcFKUsWKk5g1kS",
	"medUpPBvrlkO//i/SjYbHA3+Za9e3J5d2d4z/GDwaTjQq4INjga0LOnK/P1eTs3X9melSy7m9veLouSy",
	"5HoVvMCFZnNWujfw18jngubxB+vHVJrqauN2DPxO8U2zI6ou+xdSVTw1D2ayzKkeHOEPw/aLn4aDkv29",
	"4iVLB0d/cy8Z4Ni9+LUFW2hBKQBJuKphfV6/+Xnl9D1LtFng0yvKMzrN2M9yesq0NsvpYM4pF/OMEYXP",
	"iZwRSn6WU2JGUxEEWUie4D+b4/y6YILM+RUTQ5LxnGvAsyua8dT8t2KKaGl+U4zYQcbkjchWpFJmjWTJ",
	"9YIg0GByM7dHwQ7w28iWshmtMt1d19mCEfsQ10HUQi6FXQypFCvJ0qw9ZZqVORcw/4IrB5IxDh+MGZ/C",
	"/7Knpcw0L+xEXNQTGXwsZzRhMChLuTZbxxHt+mc0U2zYBa5esNIsmmaZXBLzaXuhhM60eWfByHs5JQuq",
	"yJQxQVQ1zbnWLB2TX2WVpYTnRbYiKcsYfpZlhH3gCgek6lKRmSxx6PdyOiRUpIaAyLzgmXmH6/G5qBF9",
	"KmXGqIAdXdGsC5+TlV5IQdiHomRKcQnAnzJi3q6oZqmBkSxT3KA7BwY7aR6dX5c/m2EXNcywx2Imuwt5",
	"zTQdpVRTOxAj98zL94KldTG+c/T2oAaD9ik9r/8y92i5oDo+iaHIqTTrJ8dAnmmmpMGQ1FDsIqMJW8gM",
	"4ME+aAMUg0qIpmbAnIqKZoSLotJkxpk5U0UWPE2ZIN9NWUIrheAdSTHC86/xQcv5PGMpkcJxA4Ob3zfO",
	"tIammfkVF5d/rrRuQSCKqi+EQWlVb9zMg0u4Z6cmUxiLTNmCXnFZdo+VPG29uuRZZlDGX6k/Z0ykrLyn",
	"cGwLVn+9CJCjeqdDWM/ErGcSHgSM28Q4u4Z7CnFuTF4DtLNVcOlqeslhp4IISTIp5qwkhVSKTzOG94YL",
	"pRlNga6K8MRwRfcC4N1z1M8AwuxzfC6emmtD8yKDQ7KzES1HUzYqAQIsJbOS5oyUVMzZkCwXPFmYg3U3",
	"h1Za5lTzBPYwk4Z+4DAqYcJ/N600Sag5FCKvWFkiMuVu75ZEKsPG4re/xedaeNNEkxi3umSr7o09TpnQ",
	"fMZZ6a+shfyQ5JXSZrmV4H+vkH9YWvve8q8oeTC3m5bzCAt7KlaEfdAlJbScV7kRDBybmBarsflQjU9l",
	"zk6QQKy++54YqOLN1ZIkJaOaISpbIrIK1lDvtQbUDpSf5zlLOdUsW5GSmaEIha2mbMYFNx8MDZ7B9GbK",
	"IcBEVtquiJaaJ1VGS3/Pesi4qqZO6lknLEXki1P7pefQO49wZj+/4nCLrjHCX82XPDNyUxspDY7ZlW0p",
	"MJ3WoGjJTdV0ZJ4gxBHnPPl6VpUlEzpbEWkkHOrGBSQOZBw1JpOfnp7+9OL5xcvjVy8uTp6e/TRB+T3l",
	"JUu0LFekoHpB/pVMzgd7/wL/Ox9MCC0Kc/3tXWSiys3+ZjxjF+Z9c9946f4JP1tZc0HVgqUX9Zu/Re5I",
	"37l0RR8LgWD3wcVEwY4qcvzcXRnYdkDAx+QXSQRTRgpQuqwSXZVMke9AsFNDkvLETEVLztT3hJaMqKoo",
	"ZKnbW7eLHxqZ//DAbDqTVA+GgNfbbjJAnQard8g4jAm9jj03OdjEfjM5IjRb0hXS9DGZ1PxqcoToAV9b",
	"0vXuGEVwAKgV3EryXcYvGaEOaISm6UiK78dksmTT2DBLNq25IWBdTgWdM0PUkNYLqZGo21kcY3svp2My",
	"QVlickQEu2IlDP2nNi5b0mhWirKheRGAA3qnmV3QrElr3GnVAMWZBkB0LFwGw8GSTTeeWRwjne5S4wlK",
	"OVwZRk7nrLSMWQNFpLlh/moLse+zJf6YqKppRCX7iapFSFaAlZHjDp1RxLLEjE5ZRpIFcnLYqxkZpRv8",
	"eUzOzM9cIbOSosYwL5IzoarSsC8rt3rFoTmpuYRVAaI61axHbIQl7aa/uwm2tj3E9NuOatjiAJYK4vKC",
	"Oe1ZbOIKBuciksMrrrQjg0DX+7Gvi2lOtb/exs8a7LZn1/UUsQ1aqnJC9eLZgiWXb5myqnRL9zdqRXfz",
	"HbVn5eQNvTAI952Q+nvLDKK3AKTi+CVDgRkwckkV2hcM5s24SHEWx0eiA6sLnDZqrkC5asH8Qi2/kqUh",
	"juOoZAQcM7pSGMQvdCYrkUbXpGRVJhvFmuBITvGD9pEi0OyK/LDhnof2wDYc+Usu0vrEt8K/HoSJmGW6",
	"+zj62JRWqFIy4VQj3Te7uWDi6oqWA4sY/VKKsz12zsM+ICUzih7I8ZQoNHRZixnQuw8sqTTbZBPtNzh6",
	"9hE8djCO053gk9ixvChLWXb38yMTrOQJYeYxKZkqpFAsZr1NI6j+09nZCUETIzFveB3BD0SODb9OsipF",
	"WwxeilUmaUqURKz2AMTVNmCbZXZpXKAxlEujvD4zkz3cP/RcxxswUqrplKJCO63UynAnRmChblGWeUmh",
	"KReEkntvmS5Xo6czzcp7+OqCUbCRmOVxkfKEaqasFQzVYM1zVOrNUTDlNdyS6ZKzdExegjrsZB87IFcg",
	"HRk0oUYCdwLDPWX5nnk3yTgTYJtJJVEyZ0b7nJOSUSXBBEJAZmMf8PJwmpEpTS7lbIYc01uNnbzaNVnn",
	"TCk6j+FeC7ng3Ov3o5h1xYR+Scv8dCs7eP3mW2b4mB/iZzl9Vxi+H9WIFNPegjwkBjvAmEBOZXLJ9PGb",
	"vdf/dnaGaIAiLgonyhxESQRbmh/VkEyKkl1xWakLxNuJNwCxD4imCMS2yJYxzS7sWbP0gka4yvHM6swZ",
	"A45lqLX/wgpPzszCc6Y0zQtiqDoilME1h0zmU6VlifLUy4zmTCTSM/rmMRuYjcyIUUYVIWLv3h0/d1Lg",
	"z+At2OBoqEWr5kC/0DzUUuM2kQa4N2GHkbe8kyR0u3iN6eF+DKFLNiuZWlyAkTlyNP4OexHU3jK1AMO1",
	"/R4Ijt3NPYUm61q+BaxDjUeZC2sAr4YG6UBuTSmoOowmCyAaVzytaIbusiXMMjfUFuw4UhoisHKDWLN1",
	"UdIEzGm95pPdgdjvZIKpI+hx5pFTzkhGlbar3BrnllRd4I1Je7w5eEUNlr83Gr19ub4j5rZrSSa6rNjE",
	"Kij2ScESPuPmZVAawdTJ03u1sVoxPbSU2dwkd7vzQq+2Mi/CBXDACTxo1i8WeM6aSNdLG19Rpd9ai2of",
	"hbMIKssaQQ3ka0ssz+m85q8OenaZccl/Kx/icKAXVT4VlGdboFW4lWOzIvCGxHQCnIuqS/svP0k/mPiM",
	"PVslMZHaE8CMz9goMS8RdgUGB2vgN9ojcEW1qNDikMqlGBrhpIQ/q2JImE5ixH0bc6JfHCwVNaPWrntt",
	"f/gJVZev5Lzv/MG7nsk5SRaVuLQMTktCCfA1LQue7DleR0opc5IypGkpvmdlKAPyIfxyJXlqxklBBmkR",
	"nBgcMhmxGDwz63E0XttVjslruvISVF5lmhcglgim4F32QUdVFIcQa1kSxCEMd3R+16hmtrH2GLaRMs4A",
	"jBvEDABHR84AanBdQcPQ/6tmpMH2vHw7wA13IQ6b+b7GST+X8TfDI67zzU3xsxh78BTOKl8RduFPshcX",
	"USs8o71EAV8gZ3S+ARW59mgYo29oCVwHSb+Ubdk32AC3ZN+bWW6ffSwA0zaXFt/ceG2XCNY1EEuouDDS",
	"Ay31OvsOV3ZKUP5opeXIfhU38Vg4RZUHJ2OivZ3pWqO1yzXQtgOMv5j0j8vfhmaYe3OhGIuYrI1Q4PRh",
	"rsL1mvedDSQwUm639s2kZ+lW/7nEB8GwK/mJf3WBeLXLx8/gi7eo+92saH7FSmX9DluQuX7q5sYZNu5K",
	"7A43LQPOQAfUEYyKKdgTlxQCIAzdVBljBZjozJWk9r1KXAq5FLgGEOmihruOdcHMiWEOEPVoF4LTfmrf",
	"e7WjBaMbmoA/R+FgZdi/1icQLGzOwRl4OD4YPX40mqfp4YP04eEP7gyOBv+vrEp3hwYQO1Nqf5iDw/Hh",
	"iGbFgu4HZxP+TL7rjP19d/+wip0cK41lfFyLb01MtmDwGo33oOWMWi17UeVUGClTVTl8hjJWyTJGFSPT",
	"imepi0IFp5IhDVSRSbiqCaoIEkh2/QmERVnDJH49mXM9IfYrMDdG/U+tA6/vQQMU/uoYiMaw4WeMYKVZ",
	"9mY2OPrbeoQ7dd4y89Wn4cc1MuNa/4nTKon7gkjh9cmovI5hJzE7uHkAzj1HkbYmQf/0trRrGHF2Zgjj",
	"zxBu3aFvEGs//YZ4/OdMJpcZV7rfeYmM2hrfaMnACA7hpiwlCStBjQRtCl2c0ohp1tKTOOTcyn8UrueF",
	"0OUq5jrqvtRxSK6Pz8b9bKtD2bd7iGjrBOqhw3DsHhLy3F6PeEyq+ZXQqaw0Bow6/dNKkU7CtOYk3hAv",
	"W3xxQXMqLpIFSy5lpdf7PE/hZeJeDsKN3AJKlssrlhKaSTHH6GwXH7JN9F9zLT2giVuqOgt/IWQ1X4Te",
	"JWAXNHDCFJwljGg5xy2mfDZjJZiO4QTBdmu+JpQsJJjsMhBayLu3r5xLJ2LLG5MzCcwNQpMwQuftq6H5",
	"KaGaCaoZOR98nFLFPu19lMJLvaqazfgHpj6dD2K6i/mgiZZlFqVCdpiGa3ZDMHzrKGCqYKSeo3hNlXKY",
	"esoylsQjX068AxNjtc2zKbMU/b2cKmerr1HYoEsgRIGOYmnWRU4/DI4GB/sHh6P9R6P9+2f3D4/uPzi6",
	"//Bf9w+O9ve7wk/3604UZ5bhQtAZz0oWklyzsJkswcvv+GrNm1qXbwf6HAUp0zSlmgL7T1OI0KTZScSs",
	"2WC8jc2UU65LWq5IbgdzCD0mr802DHXN2Icwds76OHNpdgHxJ5XiYk4mdDwdJxND1us7ZHD1kq1aZ1SU",
	"EvZxNDgtSq4ZeVny+UIbZqNYOWY5GKIHajUtmfi/pzYEQ5Zz94aVh0/hBXKq//f/umLZoAdOJ9ZY/8zr",
	"ZM0zDz1MOf3Ac6Od3N/fHw5yLvCviLupdQ38ID34fxpEH8UPS5cV6/m2X3NKqEjMMWCuToH2muFgRjn+",
	"WNBKwT/+XrEKX4MvRl6OGuA+WMVQ9aoMrEeeJjXDqWs88svqgyp6quPBLPgsiMu30QMYSvZFxKW4TjZ0",
	"y+o7JS3LXjZhHwKf8FGULiLei5TmelQKwheRxZm3kB+wlMx4xhQyXcESphQtVzEC3mJwUXP5vWeOux4/",
	"vxdEQIDo5mIO2ow4TL0Zk6fcaEICV+o+iTFtZ4eyQoJj3rNS5n7rfapSDNBnVF2q0yrPabmKJY3lRQYO",
	"PpJZ6REThxzUx+QZ+h0wOsRa213cqfnJHRI4Ys3zccQkat3EWwmVYGe2C94iHq6XEap/qxjuOWRaPDda",
	"98PhIA+Ieh+Z/DQcQDrTxXQFKX+WXUE4cm18sJYoLhoEw9MBSyJ+67JAXMvHmvrdj0ePfDb3eckzbRTy",
	"mvsMHS95dfyXFzUriSY5yNlMseZCo1EBNag+7pDwp7ak1307CkNad9lVcGrtW/GW6aoUaBwGCQSEZuqo",
	"J7fiBmxhF12pHSYQIHU/AvcFcQLqb3un0JRxzbsU8cYGHBLj0csRGAqrYjCsf1lUOpXLOFuzBoFnUsz4",
	"vCqpk1Kbm+TqJS+VfluJDZ4BrkC65yjyGwI6Mx/WgWN2PlJWIogx8RljIF5RMmNLMqOGFKshsbH6QooR",
	"pFUaLSQJ1wtMxgigTqn2odVTBrEpeaENSTdv6QVbWZFa3NNkynqDToCPYPZdupXuB6vQJRVqxkry9OQY",
	"Ek9caPG4J7QFWOwrmdC4fvDcsyTgd4abmZsGc9mPxxsNHO1Z2rsbhgccQz17an+lJXfhv20EudBLuaQR",
	"3vZGsNGSrsiV/RgD3iHtUioN8aPSXHKb4AcpKRwy9EoGqZs5BCAZxjv5aOTgTxOrYPISUwqdSLKAJB7l",
	"PF4ud98HOTtf2ZicLWVkTWAetZOmnWQOL/0wu/wio9poMyNvs8GkWhAX7CDTlV90H6LBR5tNJNa0WgPa",
	"fbnFeT2tUs5EM1jYWqesgqHWEQc3jFrH+taRvTb6dBjja1oUBsZwyu5QiNkyJOppn/7HMYc+suHVXxgr",
	"3lZCRLPy61C4ZXBxrdMupytyyVhhiJJwQmFchMo783QPtFYEeqT6hucrRlxagXu0qS/UJmGvcS4tXh/7",
	"0D6QyBeMTJbe5cYmxPqWMD2lTtPF62MmAXjPpfmvYB90IwgNHdtDMmkCYUJevzs9MxryBDIuJ1vFm7UA",
	"6aHWB6MYlvt4+WOX8NDSc21ywfqL1QqHjwx/6/kbXy3NAjQhlm7mKDZLYrvkiLdsbth2yVLree9AkqZp",
	"yZTasT6Jpb/xmyZneklLtuYa7uzpdilIF95ErXaTsT+rwollAA5UYZUTB4jhIMFE2Qsbn+Sh0LP62Gmd",
	"sqQquV753IkWBdw2iH5d9Pwp01XxVCmuNBUahc9Y2kko5Mmpke2cDg5ylxmF+GG61Noa0l5AXgrdIvu5",
	"PxHnawlq3S1E4Qni3LNeT8UpBgtZY4x1PfCSnP709ODhI7z2qsqHRPF/QDbxdAVB3kYgs0UKSGYX5RJa",
	"ulaTltETZgM3L5KfQZ1XP55LFEIHR4PDh9P9B0/uJwePp/uHh4fp/dn0wcNZsv/4hyf0/kFC9x9N76eP",
	"HuynBw8fPXn8w/70h/3HKXu4/yB9vH/whO2bgfg/2ODo/oODB+AnxtkyOZ9zMQ+nenQ4fXyQPDqcPnlw",
	"8GCW3j+cPjl8vD+bPtrff/Rk/4f95JDef/j4/uNkdkjTBw8OHh0+nN7/4XHyiP7w5OH+4yf1VAePP3UN",
	"CQ4iJ1Fqa34NpEenCFl+HZY6cOO4aibet2L9Km0TF9BwqrxShD7fMPyIHAuCBVCsr145v4odC2OYXGib",
	"eXDut0OOn58P0NjkVG4fMOAzgCiuAnS1ibXjjFRWzfegKsbIUK89rCwxOn4+6clytSizpTaNa3/JM3Za",
	"sGSjYo2DD5vHtPk21dw/Ztc1z9BK1zqVWKmna6CHdUu3EQMUZwv62jenF1RYr2czcoCqxqDglrHZydTV",
	"+6ivMTkLpIvPR74tAkq2PBJ/1F0CZ1Uw6qQuipTX0iq76IAOxyXFliNf1uOhKaMe0XtioyV+aGSFTVIb",
	"jhkdA+jMx665jTVp9GCjo8asxo437Bd2mwD+letF7YTZCtROCU+ctzIK+qEVU4ckZYWN0gc64nwi3/jZ",
	"bCt7BsfR49/pnOpwXRxeZ7zAElAHGVZFJmmK+hgGD0XNAjjYW1wNlPVxUZzXFTxA0GjArleWuCGh4VYE",
	"hFtgb/2H3zwvTAqOczU8LRCzKSmDzxxLGYZHaW0TsnndWXll5I6XPGNBBBQgmuEk9jXzm0sMqeX6MCH7",
	"tnCgvpj+PtwMWoQT+ev2hXElIN+fizVYzrJJONpeYjz/XXnulyKEa4leydLTTZpbm5Uo+KzmWDQ1QrHV",
	"6YIIPWqtquS82t8/eOTtwVY6q5TB/I6hWUs7YGQuFKb8PbAC1D3VdHdEM6gCC+8OllhvGP40HGQBgHa0",
	"tdyCq6R16lmtIfutNwwhzTVFscNmyZxW0zWViU6ZACu+z0LEEDkFIdd7Kvh2gsmZtlKclrZClKOSwZvm",
	"4Xs59VmJ5JkbEwtbzZkOn6PqBaZeqi598rT7O5NzhW4twZitw1FkPOE6W7lppwyjyMGxYh6thn4jRovA",
	"/Bv3rhlDCox9+E5LWE9j6pnL2H0vp98D7zavm1fuKcjnBKO15jkbnwvn4xNSo2lkuoL0TtBKLB+hmhSl",
	"1DKRmauU5KGFvhkEpq+3DJlN01JC5pMZuRmT0bwcsthIZSK48MbZyrctvhcbxFUTcpa//jBqLHehZfMY",
	"9kgl6h8MZRjvnCQqi3U1+tZvPRAT/TIgZqr+Kyoh9oEiQhyoJpdcpDYnYmsY+MiwLPtZTiFIO8t+9U4t",
	"W5iBqstMzvFhGBwbvn5G53H3VyMDIVoYrbZoBcW9tKyxsSnBbBPr8vkhgfbB4e//H/mvf//9P37/z9//",
	"x+//8V///vv//P0/f///w1x+qCoRxn3ALKD1HA32MHB3T8323supQjPO/YPDMbwEZpRKXF6gXHMY4OTJ",
	"Lz8aFC3U4MiIVVBM1Ug790f397Fe4gUkqrGl8jU6ITYYayiyD5oJm8kzLqxryKzkQlbaly9qrA+n8Cvc",
	"i+/cFnvsjFdKqdeOZyt4YunAi5oTDjIuqg/B9QOv9cgelQ187kbchkiwIVbEB7xuW6Z9Q72Q8Kw3xci4",
	"V2vb91aRNXU4YQ/UOuEBSGvEnKiV0iyvA77tt61KexBmmMi54Ip1xSv7ch0zTUkml6wcJVQxb7a0U7hF",
	"2RCTczzQ88GQnA+WXKRyqfCPlJZLLvDfsmBiqlLzB9PJmJz6qWReUM196fUf5T1FJmUlgA/++ObN6eRP",
	"pKwEmYB/VWYk5UpDvB8ENBguS334n6t67BepxufiqXLyJ82I2dGwsQ9y7mJ+zgfOOGgryKNtxoVjQxHF",
	"ooR8CKrI+aApbbrxzgc17HOpjDwBYs0lI5opvZeyaTW3JSoVYVRxKAZppREXF4rea56QVCZQBBgSXbKs",
	"sbNo2YS+RBTzw8X2pR6HJJEFDxXMSbvg39iMNvE1hrvFIs/sX3UyhyHeLCXc+sexEEsqmRL3NMmpTjC9",
	"gya6opkfqWOYP8PaxiA6qnYNScAjmaVBYF2zJn27TqivSe5KpJyL48YCuSIyRz41rG1lUDZsVVClWsWo",
	"O+k8UaDbdHBN5yjK2dvnysHV0bdBGv3xcx+aY2vaWN6N6iPVxBfcnDJiSExaZXj9zVLQaAjhCRjdJctg",
	"Ywa7XPaVQUP3hV9JM/1tKynKul+79XAiRC4mZ8X7jJy5+iLYWQTi25TToJ253lV3GxI+ZmOXcOHDZIIw",
	"qfFupTW+ZHeSm0iaxJDdi+nqwkUr7RK8bIMNImvdMoVth4ohkEajZWXwdEO+IkaniZUvGWD+L62TZ2zc",
	"0W7lAr5+85abytV0pGeXE982v7Nd0CTWNybsDuMv04ZGMbbs0cYERUiSk7ZJTFDK6LMqW8W9E4bQgIG9",
	"VdRo2LC4dzElqF20ceaqzOITv3v7KkxTrmcnXCuWzbwnUy5FJmm6TQRSXfrInyLm/MH++07lMzKLfCKB",
	"kjM9aiccxfTHesK7lDMU3uprJA2FaSFdnbhSmrBudmmN7pjvLBvF1euygyD+drF/x7JNd4kYXjcdfUuK",
	"5GbqO6l1ldfwmS/xCIH3TpSTlkqjKoaYZ83cYG8EigUnBmVcUdTDTjNGsvenB7Y7WWDA8J+ItCaS1gt8",
	"LqBSwXcg30gXcT1x9NZWERNSE1ZSG9nqyzm0pXazrO83lRnrxqhnXNi+IDb6FiIp7imS+OYTGGDOw/Rt",
	"INfkzRUrlyXXDGV5LisFBY1EUHXC5ZlGxYdYEbpXcm6Ly3kagHXunFTselaYRcOpwISMlhnvKeCtGyRw",
	"ByoRRa46mjOqD5QMwlISBjohKO9cYFQ+jhNx9q8LBP08KrDmkrlJY5eo3uN2VUts0KjPm+skShQXwR5b",
	"ksEJsc86larWOmS2M6j0j/X5ga2axvr/nFGkFI7v15XDoCNLzvIp4ulWIn2jWlt3AahdbTOAutyO5AZH",
	"1XAtBdVvojG1n34bRlLou+zQUdsazV5tU0+ke2l2VY7aOLreQ+xG778dGN8deAxqi7e1RdtfRr52WcSK",
	"qlhSMuCUciSkHmmWZSMqVlKwMJL5aHA4PuiD/dHfXMCskdxmecHmtl3PqO7XMhgOcq6SSCboNUPN7cI/",
	"fvmb1ZbPcKamozM2hUXm/iM75XPxpn1YjQKA1jJvD/DpyTH0XwlO4qKuuKWWdD5n5ajiN3QwrdKE3QSH",
	"/lpdndXe/DE5QhI/mc6K1pxSxlhxam1fEd+0eextYy48AdVIl+l2amAGLlomUkzD9PKNqyPl08ZTumrq",
	"aX5sQ7BBURqTp0WRcWZrNmKevDQfcrBbTVK6UhdydrFk7HIC4X7wTvN387KrTR1ZIciEghw8GC1kVZKf",
	"fjp6/brOIsbGRzXahiMPjga5JLoiEEcBbsL0AqTuo8H9H4729zFpxSp9NqUZ8Mq9tf8kWielOUk3JpIm",
	"bKRYQUuM1l3KUcag1ZSrl2OhDkWa6Qr5ImOXPWAm350PcokeB105Z8P3Y/ICrJ05o0KR8wG7YuXKjOeq",
	"4nQ7Ivn9B6ITALQn88iB5mO8ELsH1Obh2jzWjz1sQrMxbrDiNfdCU836dGqbUF6G6XXbp/lENeJgsK0W",
	"lfYVYKRLenntCoxbLHTD8pqWD19ScmjXFZShhPYj5kiZsq/I2cwoI2AcaNe9rBGov8BnJLsfK9Uh2aoV",
	"T5vkWIcEQ1FdW046YhtQFxn9x2p92FEzf9L6J1CbC9tAArmqPSwordQaoFV4FZlxwdWir3Hn8Aue59Dv",
	"b83J9llj/kwVT9YInuPPKAG83KUE8C5G9K9SbfdLZQh+sVq421QQ9RV4WppV6XNqr2Fn2r7Eba2PxRS/",
	"UGEhT9FZSYU3BWUrG0e5ctIGnROuA8c9VGUB28bYuwatmbgwAoOc1SX4jfpJFDd/U8HA+NKVEjoaWaM+",
	"oxk6leTHk3cEAze8lefFi7++eDGua9L+ePJuBL9FhIRmj8OdS2lqOh+TZ7ZpsPVmtkocUVttHw33NuWC",
	"gpu9pCKVOYEBvYlIKT4XjlJ9IdvJBt3ijM63JP01tfdIoDp2ArsDgwjNE9V0fsFT0C0eHN4/SB/9kIwY",
	"fZSOHjx89Gj0ZDp7NGJPZvtPpuzBDwmbRtQKP0Ig6m/uHLJO9HcjroWOU/M7i9lVhY8aQz6tmRqNJNtZ",
	"spr1nz5e1yEV75ISMZKcoRvcn3bApj6hlg1pyUYdykO7xwWtYglC7xQroYCELZhrWcbx8yEpqFJLWaa+",
	"hDKo1bZOiNF/nP2yNmsY1APAAGczfLXe6ULrYvDpEzReRIcf9AhJdGAA8bT6jNHcuqrwS3W0tzdz4YJc",
	"7nWLY2DMInlJy9yGwULI9GA4yHjCbBaHJ06vrg474y+Xy/FcVGNZzvfsN2pvXmSjw/H+mInxQudYTJDr",
	"rLHa3JferpX9++P9MShIsmCCFhwsMuYnzEOCk9mjBd+7OtxL2mWF5mgo8XUojlNox6eb9YdAxoQUEBjt",
	"YH/fQZUJ+J4aHRQjwPfeWw8a4u2WAfDN+eDwmkAXBqszn4qCKOgELbNijJ5pZqjPOp1J8VL/DYL+gADV",
	"Y7wQaSG5rfo9t+3vOwN2KjcbyEfBuwehPHvOzNIH7JdcpH/2SeUnmDl2Y+CO98WMwPulrESdYw7qse9E",
	"Ci/bwMYvtC4sbhBZx6nvPLg0Ev+ylGI+bp3+S24j3mVJclky8uzVseuDic4aiHtTZEkhYg5kKLedGFIU",
	"UkVOChKQI0cFvPPPMl19MWi0CqlEwOI6gMrS+vog8giLh0gMIsPSNzePR43CDN2V/tK8uENcJIa5wZHO",
	"uGB3D6f+SjMODlcaYtN1kKmFp9Zre1WP75qe1we5kahgmtIoCAReg7KNtKuvirUnt4af/xSIidlpNUY2",
	"k9c2sLsdxulFRkxN2FKKeInZ25915DsULv40bIy1onnWHKstF29CkPZBvIUeu1csLnh05YS1p/E0SZhS",
	"vvdupJpiZEgSpnLhxu6BT/9NwcTTk2OXqJZlcmnbi0CkuaDZnpUk7YFOSEGTS3PY56L/uBXTVTGirr5P",
	"P9k5pVcsWlLoZghPdKoo0wzBamg3vUL0biHlg0jHpxYyQAT6kk1pUTgjSWpUpFmVZXUfV20rjRm58u6R",
	"knd1SFFPaitWHLJWJ2hyI2CHKzKrRII3EQqxb0BvgxAxzO6tHNWPgw3Ot/fRZZt+2vvonLCf1pGkBjNs",
	"Niw3Cjg3sLPlG6wKF+Sz1oqzdVTtouJ0c3yNFh+ZMHAm90/Ypl6/3SAzjedt704xnZbWSrLOGvneYRem",
	"Rqa3+dKaBFyit0FOn+WNtv8d9bt1y2nUFu9N/u5HVZ8EtTuW1hU+/xtDr7EB9RnIWVcGaJsPyDtVJzw7",
	"oZ2m6QiZyZosOCSjvjgom2LG14xCSxfDOGLJI2RKVV29aVrKpWqkg10f4+s97o7jrr52D+eH5BtsQXUj",
	"rL7RhKx7yD/Lqc1XzrnuoOdNahxrFgRuscpIeMg7bZaYEdVseGvQpF0BtB/cP7h5GeHMU1SfDsc0nUPW",
	"HMiUddpc84Vo0hzH3tfZiqSVr05mGxglNFk45PNDwX2QkmRGNDkXtyoewQPiSmI2KQHimPXsQM1IWXbu",
	"CNZ1gIS6UPbBYvGN4X5u5hAyeyk7lwpV+y2uFui1X/d+JcES1l2vB/E0/R0vhM/2NFQU+3AsjED5y5sz",
	"zK60jfVs+kKdnqcXspov/vtC/VEuFKDVhusE2O/3bUYCUxqUUFlyc+K69s7yyDVrdEHrN8sznSx+zOSU",
	"NupUQArZzXKReM+4rQSaYfzKnbnuei4dGm4PFatoR7geuQj6yEE2MSuvbLfSyOdqw/G9garB2B2nzkKa",
	"A6B7ltM6v5wqNcIGZrhV96/mAUKvN2Ybv90QtextKxe1fTYbyzVrvWNDN2kbs42vTVoVNoQLiWtOIZ/V",
	"3BTXyNRSxEe3QhFLhmsSMmhbVxNCey7jO0OtXtPyElcagmxYS+Ouq0lScs1KTjdgPIyXm9u206DIA5y0",
	"UCdcYQEDwxQAVRwltFWpoJCZOXHze9489C7JhUGLUqLtccH8uz7lfUqTy3kpK5GOz8UvEuajeGcn7VaF",
	"E+JVVQh7Ml+xlFQFyEpC8xJc+1KkrixIThE90WvXAQ/Wz13JirAPBUv0EKs7MF6SSd1zalInsitbe9co",
	"aRnuiUITV5i1ZdsEYvJ31wsrLnNBpyFbzuiGCIhtxxUz4bULuzZJxZzp8W1rOI3WS/0sCaAaeFZsnBhW",
	"hoCKKnxmkBlEGCAFtjkRfHh3SAEIAb4EjAH8dtytbo41g35cECgmUqIkBPh2eZoR3/Y+mv/+QnO21jRk",
	"K6RsZRhyA94ZO027zkuvioHP2nKIzaXwAq+BKTSj8ZDYcD5Brn+ztTOWlYmei9riNNTgFoEWtW75l/xu",
	"VASAASrbJtegUgFJ3RqI9VSeofjxuiD8iBFmn7aS1bbCal9foB+nN8XA/baNOPUcSVBAxzxj8nV9dMnn",
	"cyOt3i7ReieQI7KUQGZA1zeJAZ0BJ0UVYEi4SLIqReVIWW0a+nwZdUDOsdgwqty2VpIfxLBrF6TfEQ/I",
	"L9I32FCdLt/frZj+vmmw9JjVr399VYy4FdMgR92uy3RaCpLrSr7ezIQfiZQEOXx993Fv2uyYH7+Zb6HP",
	"aqO//m0eyI1IXPVWYgpLVRj8/Q5jToe2PsaqYN8bmStoG+99lx6OW3qS3d2kScIKKI/FhC45s0YtICt2",
	"krtGVKCbsFutrUdu7nwAgl3v99fBq5u76GuRC2wpaxDMqFZzqRGeQQ0quP13CRWQRoEJqJkMX5eWd3sA",
	"NEklBNNaHddvWTV3uF7qwAgZj2rePeeAE6dyO1j72rY3NPV9C0j5BzcpNo/6GubF6KCNRuT9CKSYDssV",
	"9fhmQBM4qWsC/cFZpNuJzentcXUItiQONtc0WbqJfN4RVZ4xopXy4KCvHJdruumW4CLh8HsfR/uVieYa",
	"ZPWSQL0FC4ZmvMtGBK2zI9eh56mvXfXHRs5GCbce1GwmGEN0hjUzXwtNTxvDXQdJmwuymAqeK3/YLqtZ",
	"+QYeXvL/g6Bxc5O7IDHooRvZ8xm89W3wZNiLz+eLy4oIY85UWEpNdSSfOyYWUrtuKABHsyxcdQMbtpH3",
	"4juOI9FyQfVoKasstf7BUSp7ccrbnH5dUP2r+ehYP/9WBD7nkeyT87BXgjXrRGwQBvkCGQpbGLpMcGfT",
	"gURoHAUiEVxVaRetgbVEh2BnyuTcRsH1ymNgMrIdV+pZ6uHQsAT1C4V3f6UkkcLlBGQrNwVXQWtt631w",
	"1eqxKyIKnrLSPUapLwOLEFexA86ea4a3hwVw1zDtZg/ZG4r3aU4S80KFHeNcjAaxDTVvz/kU7QEai/F3",
	"fTChfbZt1hm4w5Ff7z+5eWLpV0KzktF0ZYuJW4Hhwa363vH0IARNzCGQlUxUC6J1W7lJcE0Q5XmyIFJY",
	"8/6tsZuqxW5aROoZtuildadUvP5qlWdcXProAuiWjBDA+DKNRMUCpTKiS5YF1jfsA4fUwjbIsjXeE5pl",
	"/oLXkXw1/UCgtrMf7IIoUeFlgsU0OjfTktG1NCNs/rct5QhP9kapSKwB5bYE5SvQkmj/xdh6q6k9Nujt",
	"IUGcDw9iGNYSM+/YhoXWlXKnrgz096ybI4cwsF1jMeGnkKVW9uLXjNdubCPCP8WMM+qiFT3baA/oW8y5",
	"CEjsU4mrqMkOvKu0ERD8Erq3BIbd++h6mH7a+wi/8H+scaiH7QxlyVxobUsG3Lo7LRRP7QqM7tWd/PDD",
	"zrxBuXjX2NFXio/M6na/zax1s+LfbvzidVpYbmmIvFOXKCxjVrfajDZdbQiYwX1ZR7w9Rv5zI+MwZlSx",
	"RMWVzbQ+B9v6PmUzVhLfydX12slsxub54GD/h/OBR6w6rg6UCvDv6aoUTqSvt6e8HIdhlb51bufAMRKP",
	"ZkriGErmTApGWKZgnLp+eWyZgC0AwAWjWFLAgvD/GeE0o2dUjJ6bfY7ewQCDCAyDRp0xGMqSz7mgGcxp",
	"xofWPVggPZNhQXXfYpjroF+VbRHMQ6ptlTxXA0sQyuENaEs15xiTvmlvb+zCRi/twgYbY5W2kWdkopke",
	"KV0ymjcphNfUp1yY+z3cnBj+DOdQrb7k17ArOjG0a1I82P9h0+sWHRuIaEkOxvc+jo5Q2s+NOoBhuFOm",
	"l8wiuwVnEA3ktXYbDjLzfdVl2aE7XnR2uAzKzsNIFyK8xC51ev2tdTewvjkW8VzsqpyRKTMf+vmnq8a9",
	"Q4li0nuFjog5s4mtYAjUpRGdfMvZFBs4EHAGm0/Rz3dIM1638RDu50yWCZ9mK5Jk0jZx+Ons7IQkUggM",
	"ZHfNkSQUmrSE11bbVI3zYoR9oIkmiubMSpJaukZqJJWVEfLwAwVNaPEtTDXE21TXGoycAJnKdNXLSsOc",
	"djNFrV10wdKQHL3jpC/A7yUt89O6DcsNCUb1LG9B9L5+BazQecBVHaE3o2W+IUkfp+6MwtqDBPAD6+ze",
	"R9v759N6Az6Uu9sqbNW3ErqbBlbbsiDqeMKStGIm76hlvtnUao3ZM/LFmpPfsx1T1p++68H1rSCB2886",
	"XICuWg4fegLC2hInfLigighoJENWTN8tdAojODoNzDDSPWeY1YF73+BAtJV0WmEbbsjxBsTT0Jp5C+Q7",
	"My/eHeTT7IPeKzLKxY6Vic7awPlW8CqIK6NKkxlb2o5LAZJhS/utqFf4iR/PdXFai1XbBVUETZluFau+",
	"vAW30xrvm4+rQBb4DQRWYMczn08Hbgw2m7FEO7UAuhjjCFSRJcuydnah+ZZRWylkUeVUKIwhB+EeXPBX",
	"nHarl9SlwM0dgcYA7kZhQChcrPpeTQgXSjPazsULyqv3lsTxhdBvTgq3cq6b6tpCuBeYGw3O61Iy6+Vw",
	"VI2Vb9iNneacCV3b0gA+D5TW00U0HDyGUT7Xe5rOzUnMt8vGqStab2vI0HReJ8bc5Qj2sGUBlHiHy1AJ",
	"LHatGu2qfZi/2R36RswYCkoL1MdYg3lDyPsasH45RA6qkcfJeLD5CAp7oT98rXev2/C9+Rdge0UVgSmW",
	"sGsC9ctzx43wtNnILYBd0yBoMM12+/TXCSuc3J3MWFs6kAqMaoA6g9sgSwPRhnab0ObFprPTJm72EbIN",
	"sYL+wNStXLNXPfkedSN+NV6TjbkMX+u/Z/EKvxAE8dUvwG6If4uUzlymIBQI7ckuLgianCjv8hkSJWt7",
	"aUKzzBpKL4VcQhjbu3fHz+/OJfQBMIItd71+KIk0US9+24Julpsu3C3ctr6r9hfwgri1brpraisY2WQS",
	"96kTdRsOl1gbgC7w9j7a3hg7iF5bqZR+2JtPh+7Uy7a443mUjYW8mxKf05aWtg/jscabn8g8902bwQec",
	"QMgyOKBsjdvagLL0bXC4IBPbgm0CyhV6UJsvYciK7f80NEy8IFyTGS+VHpOnYoUWGXwtbLUSDON8rkDW",
	"K9/j7Hpy51fFqS9NCtZw3G3Tqpe+79o28gpJmaZQp25ZT7PDzd/GqmR1/m4zsts+upsSIqIN1u6CsemO",
	"2IF6EXA7a5DD6J2Q0gnUvYbOhjz9TaBhpylaDw52ZXRy/Fw1TAi139r1UCdy9s+Jo0FFeQMphIZa8MJb",
	"wH7dHT8zxoqRCroub+JyzTbN3xLLa+5sm6Ym4M1v9KVel9TNQqFOyNiXdxMFN1Cur4oRN8ZJNyGDy9Fu",
	"n+K1LVO+L/ZXtUtdkzYZAU6WzrLW6CccQfOWGwN7D7JyhH+vk9/wRS9v39z5vw36Ia6zPkniVn+rphkH",
	"CZb2i+sdd8rdibFzy2+YVzqKQkdGq4/EsLz6SxVBKqPvjeRstkb04nPxZjbbygVz92BpO4QCiW30Bv0b",
	"tBttlUgNdF6qSN3efC3An9Esw2hPZ53RkmTWDefKnIL5Ti/Y6l7JyBxK0djhx72nIjYcirjRq22n6L/U",
	"OdM0pZp+BWNr2Oz/D3Glt0bDp5VeMKEhq8D16TPY4EJR+6wFn42TGMitJcxgc5hlwKl4feBRjNU2kTgq",
	"GAenNvjayAErddqND+LoFUiFJP1f3G2s2h1DXIaca+rPSsw6EaseIPSiwgjfTPtJWOew0sFN23z8RDGt",
	"pfZfKI+nO0uof2DKY6m6PTdnT4awhMQbFxShiSEbGUuxtiMmnlmKMmrGRDl0Ad8qF3XCk6UyrBxlMqEZ",
	"EDiaqS9N1a5YYzdVzL0EwUFr+KyVx23c+M3V17WG996wbihXF7R76SNXv0hXT9WntfoiY4Hd48H+4Rds",
	"fYgo1ouYJ6x0nWeeM8GRdNr6B3HTOYbQWZZHE82v0BLLwD3qamxlmVyir8KCxW695POFJkIubQDf4e0y",
	"GHeRqICcPnTgGSkcVoeZeZDxP5fQ0t5mtuCF2/HSWvcg9eMH0Nh0mwCnnMJZxpsCRSPo+q+LGRLtb99C",
	"MKrdSd91tLIRF7hEFxh4LauGHasbfRq7JXWOh2p47BwmubKeStp8OD92XZrutg0mn8mcGkZddTkkelXw",
	"BGIPbbcmEJiLUs5LptQQ2jm5BheyJDPKs6pkGzmM4yuKibThqDPgdqND9W1Wss03ZS+nqxEflVV/WOlr",
	"urKmlEp8E0kpr+nqL4wVb9Hj/I2pZxj4bcWYOvs7kJgD13vAoMpKkD1yyVjhXPF1ADh5U7jaUZCISLlQ",
	"hBJ0tYcyqXfKxPzvPYjckehB2QtW1loTV3VU+nrUlpUuKj0qSplWyTpB3xDLN/DyiXv3TjAHqPm1975g",
	"812zsYf220LMv1Yi98GWidwg/dkUZdf248H9+zd/0V4xMdcLX/zoT2HnuJSn2C/cUFlKLAhG9hPMy7cr",
	"Pbz5lZ7QFeTrQts6Wtp+Xw/uP7wNN4KqikKW5qBes5RTcrYqrMcMUIwgRjlhcurTzesusGH014ODJ7fT",
	"YdDVv0BOCaRDSuwwNTMX2xbas25pvSil1hmz5fj+UJIH5rkbQOdSaVKyBLP/felA2C/KA0G2OwfgYN8p",
	"83HtCGFCYe0/zKEA6d2esvnyniIpnzMFxYPbZ0ye+eoDECd28suPAOefT178SCwqmUGLjAoRj9NaJ/Do",
	"RZVPBeWZ2itKdsXZ0pElXmLBREftCVJ/JwYBRMsrR82rMhscDfYGgRGqTayOm0FQnbZgDlM8O4AklW4h",
	"kZ/l1JlJQUb7e8VKbtCvbnc6bLWjGDeqaKrIoE9Pjpv9IUMTmczzSqC4CQVK2ksftx24kQksNrz2ayJP",
	"T46H/d2ZsZmV2Ya5K6XM3Io6k4HTMVIqB8sP+FmAT9S1EywEfc/K93LqK8KFc9hyB59++/R/AgAA//9n",
	"Qwu3RhEBAA==",
}

// GetSwagger returns the content of the embedded swagger specification file
// or error if failed to decode
func decodeSpec() ([]byte, error) {
	zipped, err := base64.StdEncoding.DecodeString(strings.Join(swaggerSpec, ""))
	if err != nil {
		return nil, fmt.Errorf("error base64 decoding spec: %s", err)
	}
	zr, err := gzip.NewReader(bytes.NewReader(zipped))
	if err != nil {
		return nil, fmt.Errorf("error decompressing spec: %s", err)
	}
	var buf bytes.Buffer
	_, err = buf.ReadFrom(zr)
	if err != nil {
		return nil, fmt.Errorf("error decompressing spec: %s", err)
	}

	return buf.Bytes(), nil
}

var rawSpec = decodeSpecCached()

// a naive cached of a decoded swagger spec
func decodeSpecCached() func() ([]byte, error) {
	data, err := decodeSpec()
	return func() ([]byte, error) {
		return data, err
	}
}

// Constructs a synthetic filesystem for resolving external references when loading openapi specifications.
func PathToRawSpec(pathToFile string) map[string]func() ([]byte, error) {
	var res = make(map[string]func() ([]byte, error))
	if len(pathToFile) > 0 {
		res[pathToFile] = rawSpec
	}

	return res
}

// GetSwagger returns the Swagger specification corresponding to the generated code
// in this file. The external references of Swagger specification are resolved.
// The logic of resolving external references is tightly connected to "import-mapping" feature.
// Externally referenced files must be embedded in the corresponding golang packages.
// Urls can be supported but this task was out of the scope.
func GetSwagger() (swagger *openapi3.T, err error) {
	var resolvePath = PathToRawSpec("")

	loader := openapi3.NewLoader()
	loader.IsExternalRefsAllowed = true
	loader.ReadFromURIFunc = func(loader *openapi3.Loader, url *url.URL) ([]byte, error) {
		var pathToFile = url.String()
		pathToFile = path.Clean(pathToFile)
		getSpec, ok := resolvePath[pathToFile]
		if !ok {
			err1 := fmt.Errorf("path not found: %s", pathToFile)
			return nil, err1
		}
		return getSpec()
	}
	var specData []byte
	specData, err = rawSpec()
	if err != nil {
		return
	}
	swagger, err = loader.LoadFromData(specData)
	if err != nil {
		return
	}
	return
}
